package com.pshailu.gradleTestJenkings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradleTestJenkingsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GradleTestJenkingsApplication.class, args);
		
		System.out.println("Hello World");
		System.out.println("Hello World Jenkings");
	}

}
